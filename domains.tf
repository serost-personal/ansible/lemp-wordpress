
locals {
  domain = "ostrikov.org"
}

data "aws_route53_zone" "zone" {
  name = local.domain
}

resource "aws_route53_record" "wp" {
  zone_id = data.aws_route53_zone.zone.id
  name    = "nginx-test.${local.domain}"
  type    = "A"
  ttl     = 300
  records = [aws_instance.hello_world.public_ip]
}
